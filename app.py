import os
import shlex
import shutil
import zipfile
from glob import glob
from werkzeug.utils import secure_filename
from flask import Flask, redirect, request, render_template
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
from google.cloud import storage
from subprocess import Popen, PIPE
from multiprocessing import Process
from tempfile import TemporaryDirectory

PREFIX = os.environ.get('LAMMPS_EXEC_PREFIX', 'mpiexec')
EXEC = os.environ.get('LAMMPS_EXEC', 'lmp_mpi')
GS_BUCKET_NAME = os.environ.get('GS_BUCKET_NAME', 'restful-lammps-dev')

gcs = storage.Client()
bucket = gcs.get_bucket(GS_BUCKET_NAME)

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/test.db'
db = SQLAlchemy(app)

class LAMMPSException(Exception):
    pass


class Task(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    gs_path = db.Column(db.String(80), unique=True)
    created = db.Column(db.DateTime)
    completed = db.Column(db.DateTime)
    log_url = db.Column(db.String)
    results_url = db.Column(db.String)

    def __init__(self, gs_path):
        self.created = datetime.now()
        self.gs_path = gs_path


def generate_gs_path():
    now = datetime.now()
    return now.strftime('%Y%m%d%H%M%S%f')


def call_lammps(infile=None, datafile=None, nproc=0, echo='both', log='log.lammps',
         prefix=PREFIX, exe=EXEC, gs_path=None):

    if not infile:
        raise LAMMPSException('LAMMPS input file not provided')

    if not gs_path:
        gs_path = generate_gs_path()

    filename = secure_filename(infile.filename)

    cmd = f'{prefix} '
    if nproc:
        cmd += f'-n {nproc} '
    cmd += f'{exe} -e {echo} -l {log} -i {filename}'

    with TemporaryDirectory() as tempdir:
        os.chdir(tempdir)
        infile.save(filename)
        p = Popen(shlex.split(cmd), stdin=PIPE, stdout=PIPE, stderr=PIPE)
        stdo, stde = p.communicate()
        with open('std.o', 'wb') as f:
            f.write(stdo)
        with open('std.e', 'wb') as f:
            f.write(stde)
        with zipfile.ZipFile('results.zip', 'w') as z:
            for file in glob('*'):
                if file == 'results.zip':
                    continue
                blob = storage.Blob(gs_path+'/'+file, bucket)
                with open(file) as f:
                    print(f)
                    blob.upload_from_file(f)
                    blob.make_public()
                    z.write(file)
        blob = storage.Blob(gs_path+'/results.zip', bucket)
        blob.upload_from_filename('results.zip')
        blob.make_public()

    task = Task.query.filter_by(gs_path=gs_path).first()
    task.completed = datetime.now()
    task.log_url = bucket.get_blob(gs_path+'/log.lammps').public_url
    task.results_url = bucket.get_blob(gs_path+'/results.zip').public_url
    db.session.commit()


@app.route('/')
def home():
    return 'Home'


@app.route('/submit/', methods=['GET', 'POST'])
def submit():
    if request.method == 'GET':
        return render_template('submit.html')
    else:
        gs_path = generate_gs_path()
        infile = request.files.get('infile')
        datafile = request.files.get('datafile')
        nproc = request.form.get('nproc')
        kwargs = dict(
            infile=infile,
            datafile=datafile,
            gs_path=gs_path,
            nproc=nproc
        )
        p = Process(target=call_lammps, kwargs=kwargs)
        p.start()
        task = Task(gs_path)
        db.session.add(task)
        db.session.commit()
        return redirect('/status/')


@app.route('/run/')
def run():
    gs_path = generate_gs_path()
    kwargs = dict(
        infile='melt.in',
        gs_path=gs_path
    )
    p = Process(target=call_lammps, kwargs=kwargs)
    p.start()
    task = Task(gs_path)
    db.session.add(task)
    db.session.commit()
    return f'Simulation started: <a href="/status/{gs_path}/">Status</a>'


@app.route('/delete/<gs_path>/')
def delete(gs_path):
    task = Task.query.filter_by(gs_path=gs_path).first()
    db.session.delete(task)
    db.session.commit()
    for blob in bucket.list_blobs(prefix=gs_path):
        blob.delete()
    return redirect('/status/')


@app.route('/status/')
@app.route('/status/<gs_path>/')
def status(gs_path=None):
    if gs_path:
        task = Task.query.filter_by(gs_path=gs_path).first()
        if task.completed:
            log = bucket.get_blob(gs_path+'/log.lammps')
            return log.public_url
        else:
            return ''
    else:
        tasks = Task.query.all()
        return render_template('status.html', tasks=tasks)
