# RESTful LAMMPS

This is the beginning of a project to create a RESTful(like) API capable of spawning LAMMPS simulations on a server,
collecting results, and storing them using Google Cloud Storage which can subsequently be retrieved. The goal is for
this to be included in a docker image that will act as a microservice for an application stack that requires LAMMPS
simulations.
